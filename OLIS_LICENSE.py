import requests
import re
from bs4 import BeautifulSoup
import json

def OLIS_LIST_CRAWLING():
    results = []
    url = 'https://olis.or.kr/license/compareGuide.do'
    response = requests.get(url)
    html = response.text
    soup = BeautifulSoup(html,'html.parser')
    table = soup.find('table')
    header=[]
    for th in table.select('th'):
        header.append(th.text)
    tr = table.select('tr')
    results.append(header)
    for items in tr:
        row = []
        for item in items.select('td'):
            row.append(item.text)
        results.append(row)
    results = list(filter(None, results))
    return results

def OLIS_COMPAT_GPL():
    results = []
    url = 'https://olis.or.kr/license/compareGuide.do'
    response = requests.get(url)
    html = response.text
    soup = BeautifulSoup(html,'html.parser')
    table = soup.findAll('table')[1]
    header=[]
    for th in table.select('th'):
        header.append(th.text)
    tr = table.select('tr')
    results.append(header)
    for items in tr:
        row = []
        for item in items.select('td'):
            row.append(item.text)
        results.append(row)
    results = list(filter(None, results))
    return results
