import requests
import re
from bs4 import BeautifulSoup
import json

#ID	NAME_SHORT	NAME	FSF Free/Libre?	OSI Approved?	EXCEPTION	DEPRECATED	DEPRECATED_VSERION	URL	NOTICE																		
def LICENSE_LIST():
    url = 'https://spdx.org/licenses/licenses.json'
    response = requests.get(url)
    licensesJson = response.json()
    version = licensesJson['licenseListVersion']
    releaseDate = licensesJson['releaseDate']
    licenses = licensesJson['licenses']
    results = []
    for license in licenses:
        licenseId =  license.get('licenseId')
        reference = "https://spdx.org/licenses/"+re.sub('.\/','', license.get('reference'))
        fullname = license.get('name')
        isDeprecatedLicense = int(license.get('isDeprecatedLicenseId'))
        isOSIApproved = int(license.get('isOsiApproved'))
        if license.get('isFsfLibre'):
            isFsfLibre = int(license.get('isFsfLibre'))
        else:
            isFsfLibre = 0
        isLicenseException = 0
        licenseText = LICENSE_DETAILS_TEXT(license.get('detailsUrl'), 'licenseText')
        DeprecatedVersion = GET_DEPRECATED_VERSION(reference, 'LICENSE') if isDeprecatedLicense == 1 else ""
        results.append([licenseId,fullname,isFsfLibre, isOSIApproved, isLicenseException, isDeprecatedLicense, DeprecatedVersion, reference,licenseText])
    results.append([version, releaseDate])
    return results

def LICENSE_EXCEPTION_LIST():
    url = 'https://spdx.org/licenses/exceptions.json'
    response = requests.get(url)
    licensesJson = response.json()
    version = licensesJson['licenseListVersion']
    releaseDate = licensesJson['releaseDate']
    exceptions = licensesJson['exceptions']
    results = []
    for exception in exceptions:
        licenseExceptionId =  exception.get('licenseExceptionId')
        reference = "https://spdx.org/licenses/"+re.sub('.\/','', exception.get('reference'))
        fullname = exception.get('name')
        isDeprecatedLicense = int(exception.get('isDeprecatedLicenseId'))
        isLicenseException = 1
        licenseText = LICENSE_DETAILS_TEXT(exception.get('detailsUrl'), 'licenseExceptionText')
        DeprecatedVersion = GET_DEPRECATED_VERSION(reference, 'LICENSE_EXCEPTION') if isDeprecatedLicense == 1 else ""
        results.append([licenseExceptionId,fullname,0,0, isLicenseException, isDeprecatedLicense, DeprecatedVersion,reference, licenseText])
    results.append([version, releaseDate])
    return results

def LICENSE_DETAILS_TEXT(url, func):
    response = requests.get(url)
    licensesJson = json.loads(response.text) 
    licenseText =json.dumps(licensesJson[func])
    return licenseText

def GET_DEPRECATED_VERSION(url, type):
    response = requests.get(url)
    html = response.text
    soup = BeautifulSoup(html, 'html.parser')
    DeprecatedData= soup.find('div', attrs={'style':'color:red'}).get_text()
    if type == 'LICENSE':
        version = re.compile('^This license has been deprecated since license list version (.*)')
        m = version.search(DeprecatedData)
    else:
        version_exception = re.compile('^This exception has been deprecated since license list version (.*)')
        m = version_exception.search(DeprecatedData)
    return m.group(1)[:-1]

