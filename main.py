import argparse
import csv
import re
import pandas as pd
from SPDX_LICENSE import LICENSE_LIST, LICENSE_EXCEPTION_LIST
from OPENSOURCE_ORG import OPENSOURCEORG_LIST_CRAWLING
from OLIS_LICENSE import OLIS_LIST_CRAWLING, OLIS_COMPAT_GPL

def SPDX_LICENSELIST():
    license_results = LICENSE_LIST()
    license_results = license_results[:-1]
    license_results.extend(LICENSE_EXCEPTION_LIST())
    version=license_results[-1][0]
    releaseDate=license_results[-1][1]
    license_results = license_results[:-1]
    OUTPUT_TO_CSV('SPDXLicense', version, releaseDate, license_results, False)

def OPENSOURCEORG():
    license_results = OPENSOURCEORG_LIST_CRAWLING()
    OUTPUT_TO_CSV('OpensourceORG', '', '',license_results, True)

def OLIS_LICENSELIST():
    license_results = OLIS_LIST_CRAWLING()
    OUTPUT_TO_CSV('OLIS', '', '',license_results, True)

def OLIS_COMPAT_GPL_LIST():
    license_results = OLIS_COMPAT_GPL()
    OUTPUT_TO_CSV('OLIS_COMPAT_GPL', '', '',license_results, True)

def OUTPUT_TO_CSV(filename, version, releaseDate, Data, header):
    file = filename+'_'+version+'('+releaseDate+')'+'.csv'
    if header is False:
        dataframe = pd.DataFrame(Data)
        # dataframe.index += 1
        dataframe.to_csv(file, header=False, index=1, sep='\t')
    else:
        head = Data[0]
        Data = Data[1:]  
        dataframe = pd.DataFrame(Data)  
        dataframe.index += 1
        dataframe.to_csv(file, index=1, sep='\t', header=head)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="License Crawler")
    parser.add_argument('--spdx', help="spdx", action='store_true')
    parser.add_argument('--osg', help="spdx", action='store_true')
    parser.add_argument('--olis', help="spdx", action='store_true')
    parser.add_argument('--oliscompat', help="spdx", action='store_true')

    args = parser.parse_args()
    if args.spdx:
        SPDX_LICENSELIST()
    if args.osg:
        OPENSOURCEORG()
    if args.olis:
        OLIS_LICENSELIST()
    if args.oliscompat:
        OLIS_COMPAT_GPL_LIST()
