import requests
import re
from bs4 import BeautifulSoup
import json

def OPENSOURCEORG_LIST_CRAWLING():
    url = 'https://opensource.org/licenses/alphabetical'
    response = requests.get(url)
    html = response.text
    soup = BeautifulSoup(html,'html.parser')
    regx_url = '/licenses/'
    results = []
    license_url_list = []
    url_tmp=[]
    for href in soup.find('div',{"class":"field-items"}).findAll('a', href=re.compile(regx_url)):
        license_url = href["href"]
        if "do-not-use" in license_url:
            continue
        if "category" in license_url:
            continue
        license_text = href.text
        if "(" not in license_text:
            NAME = license_text
            NAME_SHORT = ''
        elif license_text.rfind("("):
            license_text=license_text.rstrip()
            tok = license_text.rfind("(")
            NAME = license_text[0:tok]
            NAME_SHORT = license_text[tok+1:len(license_text)-1]
        else:
            pass
        if not 'https://' in license_url:
            license_url = 'https://opensource.org' + license_url
        if license_url in url_tmp:
            continue
        url_tmp.append(license_url)
        license_url_list.append([NAME_SHORT, NAME, license_url])
    license_url_list.append(['','Unicode Data Files and Software License','https://opensource.org/node/1077'])
    results = DETAIL_CRAWLING(license_url_list)
    return results

def DETAIL_CRAWLING(url_list):
    # regex_title = re.compile('[A-Za-z0-9/\.\-\+\,_ ]+ \([ A-Za-z0-9/\.\,\-\+_]+\)')
    results=[]
    results.append(['NAME_SHORT','NAME','NAME_SHORT_IN_LICENSE','NAME_SHORT_IN_LINK', 'NAME_IN_LICENSE', 'SPDX_NAME_SHORT','URL','NOTICE'])
    
    for item in url_list:
        NAME_SHORT_IN_URL  = item[2].split("/",4)[-1]
        response = requests.get(item[2])
        html = response.text
        soup = BeautifulSoup(html,'html.parser')
        NAME_SHORT_IN_LICENSE=""
        NAME_IN_LICENSE=""
        SPDX_NAME_SHORT=""
        #FIND NAME
        if soup.find('h1',attrs={'class':'page-title'}):
            title = soup.find('h1',attrs={'class':'page-title'}).get_text()
            if "(" in title:
                title=title.rstrip()
                if ((title.index(")")+1)==len(title)):
                    tok = title.index("(")
                    NAME_IN_LICENSE = title[0:tok]
                    NAME_SHORT_IN_LICENSE = title[tok+1:len(title)-1]
                else:
                    NAME_IN_LICENSE = title
                    NAME_SHORT_IN_LICENSE = ''
            else:
                NAME_IN_LICENSE = title
                NAME_SHORT_IN_LICENSE = ''

        else: 
            continue
        #Find SPDX_SHORT_ID
        spdx_id_regex = re.compile('SPDX short identifier: (.*)')
        if soup.find('p', attrs={'style':'font-weight:bold'}):
            spdx_id_html = soup.find('p', attrs={'style':'font-weight:bold'}).get_text()
            m = spdx_id_regex.search(spdx_id_html)
            SPDX_NAME_SHORT = m.group(1)
        elif soup.find('span', attrs={'style':'font-family:courier new,courier,monospace; font-size:16px;'}):
            SPDX_NAME_SHORT = soup.find('span', attrs={'style':'font-family:courier new,courier,monospace; font-size:16px;'}).get_text()
        elif soup.find('span', attrs={'style':'font-family:courier new,courier,monospace;'}):
            SPDX_NAME_SHORT = soup.find('span', attrs={'style':'font-family:courier new,courier,monospace;'}).get_text()
        # elif soup.find('div')
        else:
            SPDX_NAME_SHORT=''
        
        NOTICE = ""
        #FIND NOTICE(ONLY P TAG without no style)
        body = soup.find('div', attrs={'class':'field-item even'})
        # if "SPDX short identifier: " in body.get_text():
        #     SPDX_NAME_SHORT=body.get_text().split("SPDX short identifier: ")[1]
        if body.find('div', attrs={'id':'LicenseText'}):
            NOTICE += body.find('div', attrs={'id':'LicenseText'}).get_text()
        elif body.find('pre'):
            NOTICE +=body.find('pre').get_text()
        elif body.find('p'):
            for line in body.find_all('p'):
                if spdx_id_regex.search(line.text):
                    SPDX_NAME_SHORT = spdx_id_regex.search(line.text).group(1)
                elif "SPDX Short identifier: " in line.text:
                    SPDX_NAME_SHORT = line.text.split("SPDX Short identifier: ")[1]
                else:
                    NOTICE += line.text
        else:
            NOTICE +=body.text
        results.append([item[0],item[1],NAME_SHORT_IN_LICENSE,NAME_SHORT_IN_URL, NAME_IN_LICENSE, SPDX_NAME_SHORT,item[2],NOTICE])
        print(results[-1])
    return results



# OPENSOURCEORG_LIST_CRAWLING()